void main() {
  MTNApp mtnapp = MTNApp();
  mtnapp.printDetails();
}

class MTNApp {
  String AppName = "Ambani Afrika";
  int YearWon = 2021;
  String Developer = "Mukundi Lambani";
  String Sector = "Education";
  void printDetails() {
    print(
        "The apps name is ${AppName.toUpperCase()} and the sector is ${Sector} and the developer is ${Developer} and the year it won is ${YearWon}");
  }
}
